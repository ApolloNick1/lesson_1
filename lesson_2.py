from statistics import mode


def most_common_numbers():
    with open('lesson_2.txt') as file:
        lines = file.readlines()
        return mode(list(lines))


print(most_common_numbers())
