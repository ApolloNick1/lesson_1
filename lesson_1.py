import functools
import time


def cache(n):
    def save_cache(function):

        cache_obj = {}
        cache_time = {}

        @functools.wraps(function)
        def wrapper(*args):
            if args not in cache_obj:
                cache_time["time"] = time.time()
                cache_obj[args] = function(*args)
                return cache_obj[args]
            else:
                measure_time = time.time() - cache_time[args]
                if measure_time < n:
                    return cache_obj[args]
                else:
                    cache_time["time"] = time.time()
                    cache_obj[args] = function(*args)
                    return cache_obj[args]
        return wrapper
    return save_cache
